# Module:- Machine Learning
## Train your favorite  ml model to calculate y=5x+3

* Generated X using random values of array and y from the above equation
* Added noice & outlier to y
* splitted dataframe into train and test sets
* Trained Linear regression, Decision Tree regression, Random Forest, KNN, SVM, Bayesian Regression, Ridge regression and lasso regression 
* Calculated their scores
* also calculated coefficients and intercept for Linear regression model
* At the end of the notebook, plotted two different graphs:
    1. result of algorithms test prediction
    2. algorithms training time
    3. algorithms score 
     
* Adding noice to the data make a little change in slope of the models and huge difference in intercept.
* In-order to overcome above point we need to do some pre-processing for data i.e Normalizing, removing outliers with qurtile range.